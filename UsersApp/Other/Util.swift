//
//  Util.swift
//  UsersApp
//
//  Created by Chernousov Alexander on 21.11.2017.
//  Copyright © 2017 Chernousov Alexander. All rights reserved.
//

import Foundation

struct Pair<T1, T2> {
    var first: T1
    var second: T2
    
    init(_ first: T1, _ second: T2) {
        self.first = first
        self.second = second
    }
}

extension String {
    fileprivate var skipTable: [Character: Int] {
        var skipTable: [Character: Int] = [:]
        for (i, c) in enumerated() {
            skipTable[c] = count - i - 1
        }
        return skipTable
    }
    
    fileprivate func match(from currentIndex: Index, with pattern: String) -> Index? {
        if currentIndex < startIndex { return nil }
        if currentIndex >= endIndex { return nil }
        
        if self[currentIndex] != pattern.last { return nil }
        
        if pattern.count == 1 && self[currentIndex] == pattern.last { return currentIndex }
        return match(from: index(before: currentIndex), with: "\(pattern.dropLast())")
    }
    
    func index(of pattern: String) -> Index? {
        let patternLength = pattern.count
        guard patternLength > 0, patternLength <= count else { return nil }
        let skipTable = pattern.skipTable
        let lastChar = pattern.last!
        
        var i = index(startIndex, offsetBy: patternLength - 1)
        
        while i < endIndex {
            let c = self[i]
            
            if c == lastChar {
                if let k = match(from: i, with: pattern) { return k }
                i = index(after: i)
            } else {
                i = index(i, offsetBy: skipTable[c] ?? patternLength, limitedBy: endIndex) ?? endIndex
            }
        }
        return nil
    }
}

class Util {
    
    static func getStringInRange(startIndex: Int, endIndex: Int, innerString: String) -> String{
        
        var tempString = innerString
        tempString.removeFirst(startIndex)
        tempString.removeLast(innerString.count - (endIndex))
        
        return tempString
    }
    
    static func createString(from requestParameters: [Pair<String, String>]) -> String {
        var str = ""
        for parameter in requestParameters {
            str += "\(parameter.first)=\(parameter.second)&"
        }
        str.removeLast()
        return str
    }
    
}
