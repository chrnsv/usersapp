//
//  VKInfo.swift
//  UsersApp
//
//  Created by Chernousov Alexander on 25.11.2017.
//  Copyright © 2017 Chernousov Alexander. All rights reserved.
//

import Foundation

enum VK: String {
    
    case appID = "6265306"
    case authURL = "https://oauth.vk.com/authorize?client_id=6265306&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=friends,offline&response_type=token&v=5.52"
    case errorAuthURL = "https://oauth.vk.com/blank.html#error=access_denied&error_reason=user_denied&error_description=User%20denied%20your%20request"
    case apiURL = "https://api.vk.com/method/"
    case apiVersion = "5.69"
    
}

enum VKMethod: String {
    
    case getFriends = "friends.get"
    case getUsers = "users.get"
    case searchUsers = "users.search"
}
