//
//  UsersTableViewController.swift
//  UsersApp
//
//  Created by Chernousov Alexander on 18.11.2017.
//  Copyright © 2017 Chernousov Alexander. All rights reserved.
//

import UIKit

enum SegueTypes: String {
    case profile = "ProfileSegue"
    case myProfile = "MyProfileSegue"
    case friends = "FriendsSegue"
    case friendProfile = "FriendProfileSegue"
}

class UsersTableViewController: UITableViewController {
    
    var users: [User] = []
    var isUsers = false
    var myProfile : User!
    
    
    let searchManager = SearchManager()
    var searchText = ""
    
    private var shouldShowLoadingCell = false
    
    var searchController: UISearchController!
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.hidesBarsOnSwipe = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        searchController.isActive = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if myProfile == nil {
            ProfileManager.getMyProfileInfo(completion: { (me) in
                DispatchQueue.main.async {
                    self.myProfile = me
                }
            })
        }
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        tableView.tableHeaderView = searchController.searchBar
        searchController.delegate = self
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows

        return shouldShowLoadingCell ? users.count + 1 : users.count

        
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadingIndexPath(indexPath) {
            return tableView.dequeueReusableCell(withIdentifier: "LoadingCell", for: indexPath) as! LoadingTableViewCell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! UsersTableViewCell
            
            let user = userToDisplayAt(indexPath: indexPath)
            
            if (user.photoData != nil) {
                cell.userPic.image = UIImage(data: user.photoData!)
                cell.userPic.layer.cornerRadius = cell.userPic.bounds.size.width / 2
                cell.userPic.clipsToBounds = true
            }
            
            cell.fullName.text = user.getFullName()
            
            return cell
            
        }
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard isLoadingIndexPath(indexPath) else { return }
        loadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueTypes.profile.rawValue {
            if let indexPath = tableView.indexPathForSelectedRow {
                let dvc = segue.destination as! ProfileViewController
                dvc.user = userToDisplayAt(indexPath: indexPath)
            }
        }
        else if segue.identifier == SegueTypes.myProfile.rawValue {
            let dvc = segue.destination as! ProfileViewController
            dvc.user = myProfile
        }
    }
    
    private func userToDisplayAt(indexPath: IndexPath) -> User {
        let user = users[indexPath.row]
        return user
    }
    
    private func loadData(refresh: Bool = false) {
        self.searchManager.getUsers(newSearch: refresh, searchText: self.searchText) { (newUsers) in
            DispatchQueue.main.async {
                if refresh {
                    self.users = newUsers
                } else {
                    self.users.append(contentsOf: newUsers)
                }
                
                self.shouldShowLoadingCell = self.searchManager.currentPart < self.searchManager.numberOfParts
                
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
        }
        
        
        
    }
    
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldShowLoadingCell else { return false }
        return indexPath.row == self.users.count
    }
    
    @objc
    private func refreshData() {
        searchManager.currentPart = 1
        loadData(refresh: true)
    }
    

}

extension UsersTableViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchText = searchBar.text!
        loadData(refresh: true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchBar.text == "" {
            navigationController?.hidesBarsOnSwipe = false
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        navigationController?.hidesBarsOnSwipe = true
    }
    
}

extension UsersTableViewController: UISearchControllerDelegate {
    
    func didPresentSearchController(_ searchController: UISearchController) {
        
        self.searchController.searchBar.becomeFirstResponder()
    }
    
}
