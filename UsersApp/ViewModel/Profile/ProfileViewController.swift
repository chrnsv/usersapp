//
//  ProfileViewController.swift
//  UsersApp
//
//  Created by Chernousov Alexander on 18.11.2017.
//  Copyright © 2017 Chernousov Alexander. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var userPic: UIImageView!
    @IBOutlet weak var userInfoTableView: UITableView!
    
    var user: User?
    var data: [Pair<String, String>]?
    var userDataCount = 4
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.hidesBarsOnSwipe = false
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if user != nil {
            data = user!.getAsArrayOfPair()
            userDataCount = data!.count
            if (user!.photoData != nil) {
                userPic.image = UIImage(data: user!.photoData!)
            }
            self.navigationItem.title = user!.getFullName()
        } else {
            userPic.image = UIImage(named: "")
        }
        userPic.clipsToBounds = true
        
        userInfoTableView.tableFooterView = UIView(frame: CGRect.zero)
        
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data!.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == data!.count) {
            return tableView.dequeueReusableCell(withIdentifier: "LinkCell", for: indexPath)
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileTableViewCell
            cell.key.text = data![indexPath.row].first
            cell.value.text = data![indexPath.row].second
            
            return cell
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueTypes.friends.rawValue {
            let dvc = segue.destination as! FriendsTableViewController
            dvc.id = user!.id
        }
    }

}
