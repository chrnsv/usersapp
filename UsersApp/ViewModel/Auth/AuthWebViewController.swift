//
//  AuthWebViewController.swift
//  UsersApp
//
//  Created by Chernousov Alexander on 23.11.2017.
//  Copyright © 2017 Chernousov Alexander. All rights reserved.
//

import UIKit

class AuthWebViewController: UIViewController, UIWebViewDelegate {

    var webView: UIWebView! = nil
    var viewController: AuthViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if webView == nil {
            self.webView = UIWebView(frame: self.view.bounds)
            self.webView.delegate = self
            self.webView.scalesPageToFit = true
            self.view = webView
        }
        
        let url = URL(string: VK.authURL.rawValue)
        webView.loadRequest(URLRequest(url: url!))
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        webView.stopLoading()
        webView.delegate = nil
        
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let url = request.url
        
        if url?.absoluteString == VK.errorAuthURL.rawValue {
            super.dismiss(animated: true, completion: nil)
            return false
        }
        return true
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let request = webView.request?.url?.absoluteString
        if ProfileManager.setToken(by: request!) {
            viewController!.successfulAuth()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

