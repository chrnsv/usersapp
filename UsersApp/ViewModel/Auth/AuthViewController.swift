//
//  AuthViewController.swift
//  UsersApp
//
//  Created by Chernousov Alexander on 23.11.2017.
//  Copyright © 2017 Chernousov Alexander. All rights reserved.
//

import UIKit

class AuthViewController: UIViewController {

    var isAuth: Bool = false
    
    @IBAction func authButton(_ sender: UIButton) {
        
        let authWVC = AuthWebViewController()
        authWVC.viewController = self
        self.present(authWVC, animated: true, completion: nil)
        
        
    }
    
    func successfulAuth() {
        
        isAuth = true
        loadApp()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (isAuth) {
            loadApp()
        }
    }
    func loadApp() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "NavigationController")
        DispatchQueue.main.async {
            self.present(vc!,animated: true, completion:nil)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

}
