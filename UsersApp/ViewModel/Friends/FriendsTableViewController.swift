//
//  FriendsTableViewController.swift
//  UsersApp
//
//  Created by Chernousov Alexander on 25.11.2017.
//  Copyright © 2017 Chernousov Alexander. All rights reserved.
//

import UIKit

class FriendsTableViewController: UITableViewController {
    
    var users: [User] = []
    let friends = Friends()
    
    var id: Int?
    
    private var shouldShowLoadingCell = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationItem.title = "Друзья"
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        
//        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        loadData(refresh: true)

    }
    
    private func loadData(refresh: Bool = false) {
        if id != nil {
            friends.get(newSearch: refresh, by: String(id!)) { (newUsers) in
                DispatchQueue.main.async {
                    if refresh {
                        self.users = newUsers
                    }
                    else {
                        self.users.append(contentsOf: newUsers)
                    }
                    self.shouldShowLoadingCell = self.friends.currentPart < self.friends.numberOfParts
                    
                    self.refreshControl?.endRefreshing()
                    self.tableView.reloadData()
                }
                
            }
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return shouldShowLoadingCell ? users.count + 1 : users.count
        
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadingIndexPath(indexPath) {
            return tableView.dequeueReusableCell(withIdentifier: "LoadingCell", for: indexPath) as! LoadingTableViewCell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! UsersTableViewCell
            
            let user = users[indexPath.row]
            
            if (user.photoData != nil) {
                cell.userPic.image = UIImage(data: user.photoData!)
                cell.userPic.layer.cornerRadius = cell.userPic.bounds.size.width / 2
                cell.userPic.clipsToBounds = true
                
            }
            
            cell.fullName.text = user.getFullName()
            
            return cell
            
        }
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard isLoadingIndexPath(indexPath) else { return }
        loadData()
    }

    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldShowLoadingCell else { return false }
        return indexPath.row == self.users.count
    }
    
    @objc
    private func refreshData() {
        friends.currentPart = 1
        loadData(refresh: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueTypes.friendProfile.rawValue {
            if let indexPath = tableView.indexPathForSelectedRow {
                let dvc = segue.destination as! ProfileViewController
                dvc.user = users[indexPath.row]
            }
        }
    }
    
}
