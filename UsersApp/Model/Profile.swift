//
//  Profile.swift
//  UsersApp
//
//  Created by Chernousov Alexander on 24.11.2017.
//  Copyright © 2017 Chernousov Alexander. All rights reserved.
//

import Foundation

class ProfileManager {
    
    static func setToken(by string: String) -> Bool {
        var temp = string
        if temp.range(of: "access_token") != nil {
            var key = "access_token="
            let astart = temp.distance(from: temp.startIndex, to: temp.index(of: "access_token=")!)  + "access_token=".count
            let aend = temp.count - temp.distance(from: temp.index(of: "&")!, to: temp.endIndex)
            let accessToken = Util.getStringInRange(startIndex: astart, endIndex: aend, innerString: temp)
            key = "user_id="
            //get user id
            temp.removeFirst(temp.distance(from: temp.startIndex, to: temp.index(of: key)!) + key.count)
            
            let user_id = temp
            
            let userDefaults = UserDefaults.standard
            userDefaults.set(user_id, forKey: "userID")
            userDefaults.set(accessToken,forKey: "accessToken")
            userDefaults.set(true, forKey: "isAccess")
            userDefaults.synchronize()
            
            return true
        } else {
            return false
        }
    }
    
    
    
    static func getMyProfileInfo(completion: @escaping (User) -> Void) {
        
        let userDefaults = UserDefaults.standard
        let token = userDefaults.string(forKey: "accessToken")!
        
        let searchRequest = Request(url: VK.apiURL.rawValue, method: VKMethod.getUsers.rawValue, parameters: [Pair<String, String>("user_ids",userDefaults.string(forKey: "userID")!), Pair<String, String>("fields","domain,sex,photo_200_orig,relation,online"), Pair<String, String>("name_case","nom"), Pair<String, String>("access_token",token), Pair<String, String>("v",VK.apiVersion.rawValue)])
        
        guard let url = URL(string: searchRequest.url + searchRequest.method!) else {return}
        var request = URLRequest(url: url)
        request.httpMethod = RequestType.post.rawValue
        request.addValue(ContentType.urlencoded.rawValue, forHTTPHeaderField: "Content-Type")
        if (searchRequest.parameters != nil) {
            let httpBody = Util.createString(from: searchRequest.parameters!).data(using: .utf8)
            if httpBody != nil {
                request.httpBody = httpBody!
            }
        }
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data else { print("Error"); return }
            print(data)
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                
                print(json)
                let responseVK = try JSONDecoder().decode(ProfileResponseVK.self , from: data)
                print(responseVK.response)
                responseVK.response[0].photoData =  try Data(contentsOf:URL(string: responseVK.response[0].photoURL)!) 
                completion(responseVK.response[0])
                
            } catch {
                
            }
            
        }
        task.resume()
        
    }
    
}
