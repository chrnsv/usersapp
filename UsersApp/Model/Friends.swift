//
//  Friends.swift
//  UsersApp
//
//  Created by Chernousov Alexander on 25.11.2017.
//  Copyright © 2017 Chernousov Alexander. All rights reserved.
//

import Foundation

class Friends {
    
    var offset = 0
    var count = 20
    
    var currentPart = 0
    var numberOfParts = 0
    
    var friendsRequest: Request
    
    init() {
        let token = UserDefaults.standard.string(forKey: "accessToken")!
        
        friendsRequest = Request(url: VK.apiURL.rawValue,
                                     method: VKMethod.getFriends.rawValue,
                                     parameters: [Pair<String, String>("user_id",UserDefaults.standard.string(forKey: "userID")!),
                                                  Pair<String, String>("order","hints"), Pair<String, String>("offset",String(offset)), Pair<String, String>("count",String(count)),
                                                  Pair<String, String>("fields","domain,sex,photo_200_orig,relation,online"),
                                                  Pair<String, String>("access_token",token),
                                                  Pair<String, String>("v",VK.apiVersion.rawValue)])
        
        
    }
    
    func get(newSearch: Bool, by id: String, completion: @escaping ([User]) -> Void)  {
        guard let prms = friendsRequest.parameters else {return}
        if newSearch {
            guard let i = prms.index(where: { (sample) -> Bool in
                sample.first == "user_id"
            })
                else {return}
            
            friendsRequest.parameters![i].second = id
            
            offset = 0
            guard let j = prms.index(where: { (sample) -> Bool in
                sample.first == "offset"
            })
                else {return}
            friendsRequest.parameters![j].second = String(offset)
        }
        else {
            guard currentPart < numberOfParts else {return}
            guard let prms = friendsRequest.parameters else {return}
            offset += count
            guard let j = prms.index(where: { (sample) -> Bool in
                sample.first == "offset"
            })
                else {return}
            friendsRequest.parameters![j].second = String(offset)
        }
        
        
        guard let url = URL(string: friendsRequest.url + friendsRequest.method!) else {return}
        var request = URLRequest(url: url)
        request.httpMethod = RequestType.post.rawValue
        request.addValue(ContentType.urlencoded.rawValue, forHTTPHeaderField: "Content-Type")
        if (friendsRequest.parameters != nil) {
            let httpBody = Util.createString(from: friendsRequest.parameters!).data(using: .utf8)
            if httpBody != nil {
                request.httpBody = httpBody!
            }
        }
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data else { print("Error"); return }
            print(data)
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                
                print(json)
                let responseVK = try JSONDecoder().decode(ResponseVK.self, from: data)
                
                for i in 0 ..< responseVK.response.items.count  {
                    responseVK.response.items[i].photoData = try Data(contentsOf:URL(string: responseVK.response.items[i].photoURL)!)
                }
                if newSearch {
                    self.currentPart = 1
                    self.numberOfParts = responseVK.response.count / self.count
                    if (responseVK.response.count % self.count != 0) {
                        self.numberOfParts += 1
                    }
                }
                else {
                    self.currentPart += 1
                }
                completion(responseVK.response.items)
                
            } catch {
                
            }
            
        }
        task.resume()
        
    }
    
}
