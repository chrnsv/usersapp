//
//  Search.swift
//  UsersApp
//
//  Created by Chernousov Alexander on 25.11.2017.
//  Copyright © 2017 Chernousov Alexander. All rights reserved.
//

import Foundation

class SearchManager {
    
    private let token: String
    private let searchRequest: Request
    
    var offset: Int
    var count: Int
    
    var currentPart: Int
    var numberOfParts: Int
    
    var responseVK: ResponseVK?
    
    
    init() {
        
        
        let userDefaults = UserDefaults.standard
        self.token = userDefaults.string(forKey: "accessToken")!
        
        offset = 0
        count = 20
        
        currentPart = 0
        numberOfParts = 0
        
        searchRequest = Request(url: VK.apiURL.rawValue, method: VKMethod.searchUsers.rawValue, parameters: [Pair<String, String>("q",""), Pair<String, String>("sort","0"), Pair<String, String>("offset",String(offset)), Pair<String, String>("count",String(count)), Pair<String, String>("fields","domain,sex,photo_200_orig,relation,online"), Pair<String, String>("access_token",token), Pair<String, String>("v",VK.apiVersion.rawValue)])
        
    }
    
    func getUsers(newSearch: Bool, searchText: String, completion: @escaping ([User]) -> Void) {
        guard let prms = searchRequest.parameters else {return}
        if newSearch {
            guard let i = prms.index(where: { (sample) -> Bool in
                sample.first == "q"
            })
                else {return}
            
            searchRequest.parameters![i].second = searchText
            
            offset = 0
            guard let j = prms.index(where: { (sample) -> Bool in
                sample.first == "offset"
            })
                else {return}
            searchRequest.parameters![j].second = String(offset)
        }
        else {
            guard currentPart < numberOfParts else {return}
            guard let prms = searchRequest.parameters else {return}
            offset += count
            guard let j = prms.index(where: { (sample) -> Bool in
                sample.first == "offset"
            })
                else {return}
            searchRequest.parameters![j].second = String(offset)
        }
        
        
        guard let url = URL(string: searchRequest.url + searchRequest.method!) else {return}
        var request = URLRequest(url: url)
        request.httpMethod = RequestType.post.rawValue
        request.addValue(ContentType.urlencoded.rawValue, forHTTPHeaderField: "Content-Type")
        if (searchRequest.parameters != nil) {
            let httpBody = Util.createString(from: searchRequest.parameters!).data(using: .utf8)
            if httpBody != nil {
                request.httpBody = httpBody!
            }
        }
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data else { print("Error"); return }
            print(data)
            do {
                self.responseVK = try JSONDecoder().decode(ResponseVK.self, from: data)
                
                for i in 0 ..< self.responseVK!.response.items.count  {
                    self.responseVK!.response.items[i].photoData = try Data(contentsOf: URL(string: self.responseVK!.response.items[i].photoURL)!)
                }
                if newSearch {
                    self.currentPart = 1
                    self.numberOfParts = self.responseVK!.response.count / self.count
                    if (self.responseVK!.response.count % self.count != 0) {
                        self.numberOfParts += 1
                    }
                }
                else {
                    self.currentPart += 1
                }
            } catch {
                
            }
            completion(self.responseVK!.response.items)
        }
        task.resume()
        
    }
    
}
