//
//  Request.swift
//  UsersApp
//
//  Created by Chernousov Alexander on 21.11.2017.
//  Copyright © 2017 Chernousov Alexander. All rights reserved.
//

import Foundation

enum RequestType: String {
    case get = "GET"
    case post = "POST"
}

enum ContentType: String {
    case urlencoded = "application/x-www-form-urlencoded"
    case json = "application/json"
}

class Request {
    
    var url: String
    var method: String?
    var parameters: [Pair<String, String>]?
    
    init(url: String, method: String, parameters: [Pair<String, String>]) {
        self.url = url
        self.method = method
        self.parameters = parameters
    }
    
}
