//
//  UserResponse.swift
//  UsersApp
//
//  Created by Chernousov Alexander on 25.11.2017.
//  Copyright © 2017 Chernousov Alexander. All rights reserved.
//

import Foundation

struct ResponseVK: Decodable {
    var response: UsersData
}

struct ProfileResponseVK: Decodable {
    var response: [User]
}

struct UsersData: Decodable {
    var count: Int
    var items: [User]
}

class User: Decodable {
    
    var domain: String
    var firstName: String
    var id: Int
    var lastName: String
    var photoURL: String
    var relation: Int?
    var sex: Int
    var photoData: Data?
    
    enum CodingKeys: String, CodingKey {
        
        case firstName = "first_name"
        case lastName = "last_name"
        case domain
        case id
        case photoURL = "photo_200_orig"
        case relation
        case sex
        case photoData
        
        
    }
    
    func getFullName() -> String {
        return "\(firstName) \(lastName)"
    }
    
    private func getSexString() -> String {
        switch sex {
        case 1:
            return "Женский"
        case 2:
            return "Мужской"
        default:
            return ""
        }
    }
    
    func getAsArrayOfPair() -> [Pair<String, String>] {
        
        let result = [Pair<String, String>("Имя", firstName),
                      Pair<String, String>("Фамилия", lastName),
                      Pair<String, String>("screenname", domain),
                      Pair<String, String>("Пол", getSexString())]
        return result
        
    }
    
}
